import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return(
            <footer style={footerStyle}>
                <p><strong>May the Force be with you</strong></p>
            </footer>
        )
    }
}

const footerStyle = {
    position: 'fixed',
    left: '0',
    bottom: '0',
    width: '100%',
    backgroundColor: '#ff7043',
    color: 'white',
    textAlign: 'center'
}

export default Footer
